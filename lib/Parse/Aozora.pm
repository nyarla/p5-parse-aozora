package Parse::Aozora;

use strict;
use warnings;

our $VERSION = '0.0001';

use Carp qw/ confess /;
use Scalar::Util qw/ blessed /;

our $FORMATTER  = sub { return [ @_ ] };
our $PASRER     = undef;

sub parser {
    my $class = shift;
    
    if ( @_ ) {
        $PARSER = shift @_;
    }

    if ( ! defined $parser ) {
        require Parse::Aozora::Parser::PRD;
        $PARSER = Parse::Aozora::Parser::PRD->new;
    }

    return $parser;
}

sub parse {
    my ( $class, $text, $formatter ) = @_;

    if ( ref $formatter ne 'CODE' ) {
        confess "Text formatter is not CODE reference: ${formatter}";
    }

    local $FORMATTER = $formatter;
    
    return $class->parser->parse( $text );
}

1;

__END__
