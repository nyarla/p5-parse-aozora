package Parse::Aozora::Node;

use strict;
use Any::Moose '::Role';

requires qw/ name as_string /;

has parent => (
    is          => 'rw',
    does        => __PACKAGE__,
    weak_ref    => 1,
);

no Any::Moose '::Role';

1;

=head1 NAME

Parse::Aozora::Node - (Moose|Mouse) role for node object.

=head1 SYNPOSIS

    package MyNode;
    
    use strict;
    use Any::Moose;
    
    with qw/ Parse::Aozora::Node /;
    
    sub name { 'mynode' }
    
    no Any::Moose;
    __PACKAGE__->meta->make_immutable;

=head1 INTERFACE METHODS

=head2 name

    my $name = $node->name;

This method returns node name.

=head2 parent 

    my $parent = $node->parent;
    $node->parent( $parent );

This method sets or gets parent node object.

Parent node has to be node object.

This property holds weak reference.

=head2 as_string

    my $source = $node->as_string;

This method gets aozora format text of this node.

=head1 REQUIREMENT METHODS

=head2 name

    sub name { 'mynodename' }

This method has to return node name.

=head2 as_string

    sub as_string {
        # your code here
    }

This method has to return aozora format text.

=head1 AUTHOR

Naoki Okamura (Nyarla) E<lt>nyarla[ at ]thotep.netE<gt>

=head1 SEE ALSO

L<Any::Moose>

=cut

