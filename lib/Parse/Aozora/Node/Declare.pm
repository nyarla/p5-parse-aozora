package Parse::Aozora::Node::Declare;

use strict;
use warnings;

use parent qw/ Exporter /;

require Any::Moose;

our $PREFIX = 'Parse::Aozora::Node';
our @EXPORT = qw/
    node
    text phrase punctuation
    extrachar
    string ruby
/;

sub node ($;@) {
    my ( $name, @args ) = @_;

    my $class = join q{::}, $PREFIX, $name;
    
    Any::Moose::load_class($class)
        if ( ! Any::Moose::is_class_loaded($class) );

    return $class->new( @args );
}

sub text ($) {
    return node Text => ( value => $_[0] );
}

sub phrase ($$) {
    return node Phrase => ( language => $_[0], contents => $_[1] );
}

sub punctuation ($$) {
    return node Punctuation => ( type => $_[0], contents => $_[1] );
}

sub extrachar ($$$) {
    return node ExtraChar => ( type => $_[0], description => $_[1], code => $_[2] );
}

sub string ($;@) {
    my $contents = shift @_;
    return node String => ( contents => $contents, @_ );
}

sub ruby (@) {
    return node Ruby => ( @_ );
}

1;
__END__


