package Parse::Aozora::Node::ExtraChar;

use strict;
use utf8;
use Carp;
use Any::Moose;

with qw/ Parse::Aozora::Node /;

sub name { 'extrachar' }

has [qw/ type description code /] => (
    is          => 'rw',
    isa         => 'Str',
    required    => 1,
);

no Any::Moose;

sub as_string {
    my ( $self ) = @_;
    
    my $type = $self->type;
    
    if ( $type eq 'kanji' ) {
        return q{※［＃「} . $self->description . q{」、} . $self->code . q{］};
    }
    elsif ( $type eq 'char' ) {
        return q{※［＃} . $self->description . q{、} . $self->code . q{］};
    }
    else {
        Carp::confess("[ExtraChar] Unknown type: @{[ $self->type ]}");
    }
}

__PACKAGE__->meta->make_immutable;

=encoding utf-8

=head1 NAME

Parse::Aozora::Node::ExtraChar - Extra character object.

=head1 SYNPOSIS

    use Parse::Aozora::Node::Declare;
    
    my $node = extrachar 'kanji', 'てへん＋劣', '第3水準1-84-77';

=head1 PROPERTIES

=head2 type

    my $type = $node->type;

This property holds node type.

This property value is C<kanji> or <char>

=head2 description

    my $description = $node->description

This property holds extra character description.


=head2 code

    my $code = $node->code;

This property holds extra character code.

=head1 AUTHOR

Naoki Okamura (Nyarla) E<lt>nyarla[ at ]thotep.netE<gt>

=head1 SEE ALSO

L<Parse::Aozora::Node>

=cut

