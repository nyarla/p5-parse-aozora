package Parse::Aozora::Node::Phrase;

use strict;
use Any::Moose;

with qw/ Parse::Aozora::Node /;

sub name { 'phrase' }

has contents => (
    is      => 'rw',
    isa     => 'ArrayRef[Object]',
    default => sub { +[] },
    trigger => sub {
        my ( $self, $new ) = @_;
        $_->parent( $self ) for @{ $new };
    },
);

has language => (
    is          => 'rw',
    isa         => 'Str',
    required    => 1,
);

no Any::Moose;

sub unshift_contents    { my $self = shift; $self->contents([ @_, @{ $self->contents } ]) }
sub shift_contents      { my $self = shift; shift @{ $self->contents }}
sub push_contents       { my $self = shift; $self->contents([ @{ $self->contents }, @_ ])}
sub pop_contents        { my $self = shift; pop @{ $self->contents }}

sub as_string {
    my ( $self ) = @_;
    my $contents = q{};
    
    $contents .= $_->as_string for @{ $self->contents }; 

    return $contents;
}

__PACKAGE__->meta->make_immutable;

=head1 NAME

Parse::Aozora::Node::Phrase - Phrase node object.

=head1 SYNPOSIS

    use Parse::Aozora::Node::Declare;
    
    my $node = phrase japanese => [ \@contents ];
       $node->parent;
       $node->push_contents(@new);
       $node->unshift_contents(@new);
       
       $node->pop_contents;
       $node->shift_contents;

=head1 PROPERTIES

=head2 name

    my $name = $node->name;

This property returns node name.

=head2 language

    my $lang = $node->language;

This property holds phrase language.

=head2 contents

    my $contents = $node->contents;

This property holds phrase contents;

This property is ARRAY reference.

=head1 METHODS

=head2 unshift_contents

    $node->unshift_contents(@new);

=head2 shift_contents

    my $first = $node->shift_contents;

=head2 push_contents

    $node->push_contents(@new);

=head2 pop_contents

    my $last = $node->pop_contents;

=head1 AUTHOR

Naoki Okamura (Nyarla) E<lt>nyarla[ at ]thotep.netE<gt>

=head1 SEE ALSO

L<Parse::Aozora::Node>

=cut

