package Parse::Aozora::Node::Punctuation;

use strict;
use utf8;
use Carp;
use Any::Moose;

with qw/ Parse::Aozora::Node /;

sub name { 'punctuation' }

has type => (
    is          => 'rw',
    isa         => 'Str',
    required    => 1,
);

has contents => (
    is          => 'rw',
    isa         => 'ArrayRef[Object]',
    required    => 1,
    trigger     => sub {
        my ( $self, $new ) = @_;
        $_->parent($self) for @{ $new || [] };
    },
);

no Any::Moose;

sub unshift_contents    { my $self = shift; $self->contents([ @_, @{ $self->contents } ]) }
sub shift_contents      { my $self = shift; shift @{ $self->contents }}
sub push_contents       { my $self = shift; $self->contents([ @{ $self->contents }, @_ ])}
sub pop_contents        { my $self = shift; pop @{ $self->contents }}

sub as_string {
    my ( $self ) = @_;
    my $contents = q{};
       $contents .= $_->as_string for @{ $self->contents };


    if ( $self->type eq 'kaeriten' ) {
        return qq{［＃${contents}］};
    }
    elsif ( $self->type eq 'kuntenokuri' ) {
        return qq{［＃（${contents}）］};
    }
    else {
        Carp::confess "Unknown type: @{[ $self->type ]}";
    }

}

__PACKAGE__->meta->make_immutable;

=head1 NAME

Parse::Aozora::Node::Punctuation - Punctuation node object.

=head1 SYNPOSIS

    use Parse::Aozora::Node::Declare;
    
    my $node = punctuation 'kaeriten' => ['レ'];

=head1 PROPERTIES

=head2 type

    my $type = $node->type;

This property holds node type.

This property value is C<kaeriten> or C<kuntenokuri>

=head2 contents

    my $contents = $node->contents

This property holds child nodes.

=head1 METHODS

=head2 unshift_contents

    $node->unshift_contents(@new);

=head2 shift_contents

    my $first = $node->shift_contents;

=head2 push_contents

    $node->push_contents(@new);

=head2 pop_contents

    my $last = $node->pop_contents;

=head1 AUTHOR

Naoki Okamura (Nyarla) E<lt>nyarla[ at ]thotep.netE<gt>

=head1 SEE ALSO

L<Parse::Aozora::Node>

=cut

