package Parse::Aozora::Node::Ruby;

use strict;
use utf8;
use Carp;
use Any::Moose;

with qw/ Parse::Aozora::Node /;

sub name { 'ruby' }

has [qw/ target ruby left_ruby /] => (
    is          => 'rw',
    isa         => 'Parse::Aozora::Node::String',
);

has '+target' => ( required => 1 );

# (大|中|窓)見出し
has heading => (
    is  => 'rw',
    isa => 'Maybe[Str]',
);

# 強調
has emphasis => (
    is  => 'rw',
    isa => 'Maybe[Str]',
);

# 太字
has bold => (
    is  => 'rw',
    isa => 'Maybe[Bool]',
);

# 斜体
has italic => (
    is  => 'rw',
    isa => 'Maybe[Bool]',
);

# 分数
has fraction => (
    is  => 'rw',
    isa => 'Maybe[Bool]',
);

# 下線
has underline => (
    is  => 'rw',
    isa => 'Maybe[Bool]',
);

# 上付き文字
has superscript => (
    is  => 'rw',
    isa => 'Maybe[Bool]',
);

# 下付き文字
has subscript => (
    is  => 'rw',
    isa => 'Maybe[Bool]',
);

# 変体仮名
has variantkana => (
    is  => 'rw',
    isa => 'Maybe[Object]',
);

# 篆書体
has tensho => (
    is => 'rw',
    isa => 'Maybe[Bool]',
);

# 小書き
has small => (
    is  => 'rw',
    isa => 'Maybe[Bool]',
);

# 行(右|左)小書き
has lineside => (
    is  => 'rw',
    isa => 'Maybe[Str]',
);

# 横組
has horizontal => (
    is  => 'rw',
    isa => 'Maybe[Bool]',
);

# 縦中横
has hinv => (
    is  => 'rw',
    isa => 'Maybe[Bool]',
);

# ローマ数字
has roman_number => (
    is  => 'rw',
    isa => 'Maybe[Bool]',
);

# 合字
has ligature => (
    is  => 'rw',
    isa => 'Maybe[Bool]',
);

# フォントサイズ
has fontsize => (
    is => 'rw',
    isa => 'Maybe[Num]',
);

# (二重)?罫囲み
has linearborder => (
    is  => 'rw',
    isa => 'Maybe[Num]',
);

# (左)?注記 底本
has [qw/ annotation left_annotation original /] => (
    is      => 'rw',
    isa     => 'Maybe[Parse::Aozora::Node::String]',
);

no Any::Moose;

sub as_string {
    my ( $self ) = @_;

    my $target = $self->target->as_string;
    my $output = q{};
    my $ruby    = q{};

    if ( defined $self->ruby ) {
        $ruby   = $self->ruby->as_string;
        $output .= qq{｜${target}《${ruby}》};
        
    }
    elsif ( defined $self->left_ruby ) {
        $ruby   = $self->left_ruby->as_string;
        $output .= qq{［＃「${target}」の左に「${ruby}」のルビ］};
    }
    else {
        confess("Ruby text is not defined.");
    }

    # 見出し
    $output .= qq{［＃ルビの「${ruby}」は@{[ $self->heading ]}見出し］} if ( defined $self->heading );

    # 強調
    $output .= qq{［＃ルビの「${ruby}」に@{[ $self->emphasis ]}］} if ( defined $self->emphasis );

    # 太字
    $output .= qq{［＃ルビの「${ruby}」は太字］} if ( defined $self->bold );

    # 斜体
    $output .= qq{［＃ルビの「${ruby}」は斜体］} if ( defined $self->italic );

    # 分数
    $output .= qq{［＃ルビの「${ruby}」は分数］} if ( defined $self->fraction );

    # 下線
    $output .= qq{［＃ルビの「${ruby}」は下線］} if ( defined $self->underline );
    
    # 上付き文字
    $output .= qq{［＃ルビの「${ruby}」は上付き小文字］} if ( defined $self->superscript );
    
    # 下付き文字
    $output .= qq{［＃ルビの「${ruby}」は下付き小文字］} if ( defined $self->subscript );
    
    # 変体仮名
    $output .= qq{［＃ルビの「${ruby}」は底本では変体仮名で「@{[ $self->variantkana->as_string ]}」の草書］} if ( defined $self->variantkana );
    
    # 篆書体
    $output .= qq{［＃ルビの「${ruby}」は篆書体］} if ( defined $self->tensho );
    
    # 小書き
    $output .= qq{［＃ルビの「${ruby}」は小書き］} if ( defined $self->small );
    
    # 行(左|右)小書き
    $output .= qq{［＃ルビの「${ruby}」は行@{[ $self->lineside ]}小書き］} if ( defined $self->lineside );
    
    # 横組み
    $output .= qq{［＃ルビの「${ruby}」は横組み］} if ( defined $self->horizontal );
    
    # 縦中横
    $output .= qq{［＃ルビの「${ruby}」は縦中横］} if ( defined $self->hinv );
    
    # 合字
    $output .= qq{［＃ルビの「${ruby}」は合字］} if ( defined $self->ligature );
    
    # ローマ数字
    $output .= qq{［＃ルビの「${ruby}」はローマ数字］} if ( defined $self->roman_number );
    
    # フォントサイズ
    $output .= qq{［＃ルビの「${ruby}」は@{[ abs($self->fontsize) ]}段階@{[ ( $self->fontsize > 0 ) ? '大きな' : '小さな' ]}文字］} if ( defined $self->fontsize );
    
    # 罫囲み
    $output .= qq{［＃ルビの「${ruby}」は@{[ ( $self->linearborder == 2 ) ? '二重' : '' ]}罫囲み］} if ( defined $self->linearborder );

    # 注記
    $output .= qq{［＃ルビの「${ruby}」に「@{[ $self->annotation->as_string ]}」の注記］} if ( defined $self->annotation );
    
    # 左注記
    $output .= qq{［＃ルビの「${ruby}」の左に「@{[ $self->left_annotation->as_string ]}」の注記］} if ( defined $self->left_annotation );
    
    # 底本
    if ( defined $self->original ) {
            my $note = $self->original->as_string;
            my $text = ( $note eq 'ママ' ) ? 'ママ' : qq{底本では「${note}」};
            $output .= qq{［＃ルビの「${ruby}」は${text}］};
    }

    return $output;
}

__PACKAGE__->meta->make_immutable;

=head1 NAME

Parse::Aozora::Node::Ruby - Ruby text node object.

=head1 SYNPOSIS

    use Parse::Aozora::Node::Declare;
    
    my $node = ruby(
        target  => string([ phrase japanese => [ text '青空文庫' ] ]),
        ruby    => string([ phrase japanese => [ text 'あおぞらぶんこ' ] ] ),
        bold    => 1,
    );

=head1 PROPERTIES

=head2 target

This property holds L<Parse::Aozora::Node::String> object of ruby target string.

=head2 ruby, left_ruby

This property holds L<Parse::Aozora::Node::String> object of ruby text.

=head2 heading

If this property sets, this ruby node is heading node.

=head2 emphasis

If this property is sets, this ruby node is emphasis node.

=head2 bold

If this property is sets, this ruby node is bold node.

=head2 italic

If this property is sets, this ruby node is italic node.

=head2 fraction

If this property is sets, this ruby node is fraction node.

=head2 superscript

If this property is sets, this ruby node is superscript node.

=head2 subscript

If this property is sets, this ruby node is subscript node.

=head2 variantkana

If this property is sets, this ruby node is variantkana node.

=head2 tensho

If this property is sets, this ruby node is tencho type node.

=head2 lineside

If this property is sets, this ruby node is lineside samll annotation node.

=head2 horizontal

If this property is sets, this ruby node is horizontal form node.

=head2 hinv

If this property is sets, this ruby node is horizontal form in vertical form node.

=head2 ligature

If this property is sets, this ruby node is ligature node.


=head2 roman_number

If this property is sets, this ruby node is roman numerals node.

=head2 fontsize

If this property is sets, this ruby node is bigger or smaller font node.


=head2 linearborder

If this property is sets, this ruby node is linearborder node.

=head2 annotation, left_annotation

If this property is sets, this node holds annotation.

=head2 original

If this property is sets, this node holds original annotation.

=head1 AUTHOR

Naoki Okamura (Nyarla) E<lt>nyarla[ at ]thotep.netE<gt>

=head1 SEE ALSO

L<Parse::Aozora::Node>

=cut
