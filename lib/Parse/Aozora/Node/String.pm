package Parse::Aozora::Node::String;

use strict;
use utf8;
use Any::Moose;

with qw/ Parse::Aozora::Node /;

sub name { 'string' }

# (大|中|窓)見出し
has heading => (
    is  => 'rw',
    isa => 'Maybe[Str]',
);

# 強調
has [qw/ emphasis left_emphasis /] => (
    is  => 'rw',
    isa => 'Maybe[Str]',
);

# 太字
has bold => (
    is  => 'rw',
    isa => 'Maybe[Bool]',
);

# 斜体
has italic => (
    is  => 'rw',
    isa => 'Maybe[Bool]',
);

# 分数
has fraction => (
    is  => 'rw',
    isa => 'Maybe[Bool]',
);

# 下線
has underline => (
    is  => 'rw',
    isa => 'Maybe[Bool]',
);

# 上付き文字
has superscript => (
    is  => 'rw',
    isa => 'Maybe[Bool]',
);

# 下付き文字
has subscript => (
    is  => 'rw',
    isa => 'Maybe[Bool]',
);

# 変体仮名
has variantkana => (
    is  => 'rw',
    isa => 'Maybe[Object]',
);

# 篆書体
has tensho => (
    is => 'rw',
    isa => 'Maybe[Bool]',
);

# 小書き
has small => (
    is  => 'rw',
    isa => 'Maybe[Bool]',
);

# 行(右|左)小書き
has lineside => (
    is  => 'rw',
    isa => 'Maybe[Str]',
);

# 横組
has horizontal => (
    is  => 'rw',
    isa => 'Maybe[Bool]',
);

# 縦中横
has hinv => (
    is  => 'rw',
    isa => 'Maybe[Bool]',
);

# ローマ数字
has roman_number => (
    is  => 'rw',
    isa => 'Maybe[Bool]',
);

# 合字
has ligature => (
    is  => 'rw',
    isa => 'Maybe[Bool]',
);

# フォントサイズ
has fontsize => (
    is => 'rw',
    isa => 'Maybe[Num]',
);

# (二重)?罫囲み
has linearborder => (
    is  => 'rw',
    isa => 'Maybe[Num]',
);

# (左)?注記 底本
has [qw/ annotation left_annotation original /] => (
    is      => 'rw',
    isa     => 'Maybe[Parse::Aozora::Node::String]',
);

# 内容
has contents => (
    is      => 'rw',
    isa     => 'ArrayRef[Object]',
    default => sub { +[] },
    trigger => sub {
        my ( $self, $new ) = @_;
        $_->parent( $self ) for @{ $new };
    },
);

no Any::Moose;

sub unshift_contents    { my $self = shift; $self->contents([ @_, @{ $self->contents } ]) }
sub shift_contents      { my $self = shift; shift @{ $self->contents }}
sub push_contents       { my $self = shift; $self->contents([ @{ $self->contents }, @_ ])}
sub pop_contents        { my $self = shift; pop @{ $self->contents }}

sub as_string {
    my ( $self ) = @_;

    my $target = q{};
       $target .= $_->as_string for @{ $self->contents };

    # 見出し
    $target .= qq{［＃「${target}」は@{[ $self->heading ]}見出し］} if ( defined $self->heading );

    # 強調
    $target .= qq{［＃「${target}」に@{[ $self->emphasis ]}］} if ( defined $self->emphasis );
    
    # 左側に強調
    $target .= qq{［＃「${target}」の左に@{[ $self->left_emphasis ]}］} if ( defined $self->left_emphasis );

    # 太字
    $target .= qq{［＃「${target}」は太字］} if ( defined $self->bold );

    # 斜体
    $target .= qq{［＃「${target}」は斜体］} if ( defined $self->italic );

    # 分数
    $target .= qq{［＃「${target}」は分数］} if ( defined $self->fraction );

    # 下線
    $target .= qq{［＃「${target}」は下線］} if ( defined $self->underline );
    
    # 上付き文字
    $target .= qq{［＃「${target}」は上付き小文字］} if ( defined $self->superscript );
    
    # 下付き文字
    $target .= qq{［＃「${target}」は下付き小文字］} if ( defined $self->subscript );
    
    # 変体仮名
    $target .= qq{［＃「${target}」は底本では変体仮名で「@{[ $self->variantkana->as_string ]}」の草書］} if ( defined $self->variantkana );
    
    # 篆書体
    $target .= qq{［＃「${target}」は篆書体］} if ( defined $self->tensho );
    
    # 小書き
    $target .= qq{［＃「${target}」は小書き］} if ( defined $self->small );
    
    # 行(左|右)小書き
    $target .= qq{［＃「${target}」は行@{[ $self->lineside ]}小書き］} if ( defined $self->lineside );
    
    # 横組み
    $target .= qq{［＃「${target}」は横組み］} if ( defined $self->horizontal );
    
    # 縦中横
    $target .= qq{［＃「${target}」は縦中横］} if ( defined $self->hinv );
    
    # 合字
    $target .= qq{［＃「${target}」は合字］} if ( defined $self->ligature );
    
    # ローマ数字
    $target .= qq{［＃「${target}」はローマ数字］} if ( defined $self->roman_number );
    
    # フォントサイズ
    $target .= qq{［＃「${target}」は@{[ abs($self->fontsize) ]}段階@{[ ( $self->fontsize > 0 ) ? '大きな' : '小さな' ]}文字］} if ( defined $self->fontsize );
    
    # 罫囲み
    $target .= qq{［＃「${target}」は@{[ ( $self->linearborder == 2 ) ? '二重' : '' ]}罫囲み］} if ( defined $self->linearborder );

    # 注記
    $target .= qq{［＃「${target}」に「@{[ $self->annotation->as_string ]}」の注記］} if ( defined $self->annotation );
    
    # 左注記
    $target .= qq{［＃「${target}」の左に「@{[ $self->left_annotation->as_string ]}」の注記］} if ( defined $self->left_annotation );
    
    # 底本
    if ( defined $self->original ) {
            my $note = $self->original->as_string;
            my $text = ( $note eq 'ママ' ) ? 'ママ' : qq{底本では「${note}」};
            $target .= qq{［＃「${target}」は${text}］};
    }

    return $target;
}

__PACKAGE__->meta->make_immutable;

=head1 NAME

Parse::Aozora::Node::String - String node object.

=head1 SYNPOSIS

    use Parse::Aozora::Node::Declare;
    
    my $node = string [ text 'foo' ], bold => 1;

=head1 PROPERTIES

=head2 contents

    my $children = $node->contents;

This property holds child nodes.

=head2 heading

If this property sets, this node is heading node.

=head2 emphasis, left_emphasis

If this property is sets, this node is emphasis node.

=head2 bold

If this property is sets, this node is bold node.

=head2 italic

If this property is sets, this node is italic node.

=head2 fraction

If this property is sets, this node is fraction node.

=head2 superscript

If this property is sets, this node is superscript node.

=head2 subscript

If this property is sets, this node is subscript node.

=head2 variantkana

If this property is sets, this node is variantkana node.

=head2 tensho

If this property is sets, this node is tencho type node.

=head2 lineside

If this property is sets, this node is lineside samll annotation node.

=head2 horizontal

If this property is sets, this node is horizontal form node.

=head2 hinv

If this property is sets, this node is horizontal form in vertical form node.

=head2 ligature

If this property is sets, this node is ligature node.


=head2 roman_number

If this property is sets, this node is roman numerals node.

=head2 fontsize

If this property is sets, this node is bigger or smaller font node.


=head2 linearborder

If this property is sets, this node is linearborder node.

=head2 annotation, left_annotation

If this property is sets, this node holds annotation.

=head2 original

If this property is sets, this node holds original annotation.

=head1 METHODS

=head2 unshift_contents

    $node->unshift_contents(@new);

=head2 shift_contents

    my $first = $node->shift_contents;

=head2 push_contents

    $node->push_contents(@new);

=head2 pop_contents

    my $last = $node->pop_contents;

=head1 AUTHOR

Naoki Okamura (Nyarla) E<lt>nyarla[ at ]thotep.netE<gt>

=head1 SEE ALSO

L<Parse::Aozora::Node>

=cut
