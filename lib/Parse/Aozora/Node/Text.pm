package Parse::Aozora::Node::Text;

use strict;
use Any::Moose;

with qw/ Parse::Aozora::Node /;

sub name { 'text' }

has value => (
    is          => 'rw',
    isa         => 'Str',
    required    => 1,
);

no Any::Moose;

sub as_string { return $_[0]->value }

__PACKAGE__->meta->make_immutable;

=head1 NAME

Parse::Aozora::Node::Text - Text node object.

=head2 SYNPOSIS

    use Parse::Aozora::Node::Declare;
    
    my $node = text 'foo';
       $node->value;
       $node->parent;

=head1 PROPERTIES

=head2 value

    my $text = $node->value;

This property holds node value.

=head2 parent

    my $parent = $node->parent;

This property holds parent node object.

=head1 AUTHOR

Naoki Okamura (Nyarla) E<lt>nyarla[ at ]thoetp.netE<gt>

=head1 SEE ALSO

L<Parse::Aozora::Node>

=cut

    
   
