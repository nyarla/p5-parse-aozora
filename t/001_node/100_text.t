#!perl

use strict;
use warnings;

use Test::More;
use Parse::Aozora::Node::Declare;

BEGIN {
    use_ok('Parse::Aozora::Node::Text');
}

my $text = text 'foo';

isa_ok( $text, 'Parse::Aozora::Node::Text' );

is( $text->value, 'foo' );

is( $text->as_string, 'foo' );

done_testing;

