#!perl

use strict;
use warnings;

use Test::More;
use Parse::Aozora::Node::Declare;

BEGIN {
    use_ok('Parse::Aozora::Node::Phrase');
}

# instance
my $node = phrase japanese => [];
isa_ok( $node, 'Parse::Aozora::Node::Phrase' );

# languge
is( $node->language, 'japanese' );

# contents
$node->push_contents( text 'foo' );
$node->unshift_contents( text 'bar' );

is(
    $node->contents->[0]->parent,
    $node,
);

is(
    $node->contents->[1]->parent,
    $node,
);

is(
    $node->shift_contents->value,
    'bar',
);

is(
    $node->pop_contents->value,
    'foo'
);

$node->push_contents( text 'foo' );
$node->push_contents( text 'bar' );

is(
    $node->as_string,
    'foobar',
);

done_testing;

