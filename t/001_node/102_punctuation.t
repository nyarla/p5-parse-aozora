#!perl

use strict;
use warnings;
use utf8;

use Test::More;
use Parse::Aozora::Node::Declare;

BEGIN {
    use_ok('Parse::Aozora::Node::Punctuation');
}

# instance
my $node = punctuation 'kaeriten' => [ text 'レ'];

isa_ok( $node, 'Parse::Aozora::Node::Punctuation' );

# type
is(
    $node->type,
    'kaeriten',
);

# contents
$node->push_contents(text 'レ');
$node->unshift_contents(text '一');

## parent
for ( @{ $node->contents } ) {
    is(
        $_->parent,
        $node,
    );
}

is(
    $node->shift_contents->value,
    '一',
);

is(
    $node->pop_contents->value,
    'レ'
);

is(
    $node->as_string,
    q{［＃レ］},
);

is(
    punctuation('kuntenokuri' => [ text 'ノ' ])->as_string,
    q{［＃（ノ）］},
);

done_testing;

