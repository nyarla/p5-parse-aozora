#!perl

use strict;
use warnings;
use utf8;

use Test::More;
use Parse::Aozora::Node::Declare;

BEGIN {
    use_ok('Parse::Aozora::Node::ExtraChar');
}

my $node = extrachar 'kanji', 'てへん＋劣', '第3水準1-84-77';

# instnace
isa_ok( $node, 'Parse::Aozora::Node::ExtraChar' );

# type
is( $node->type, 'kanji' );

# description
is( $node->description, 'てへん＋劣' );

# code
is( $node->code, '第3水準1-84-77' );

# as_string
is( $node->as_string, q{※［＃「てへん＋劣」、第3水準1-84-77］} );

done_testing;

