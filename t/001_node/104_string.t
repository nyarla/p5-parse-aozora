#!perl

use strict;
use warnings;
use utf8;

use Test::More;
use Encode;
use Parse::Aozora::Node::Declare;

BEGIN {
    use_ok('Parse::Aozora::Node::String');
}

my $enc = find_encoding('utf-8');

my @tests = (
    'ほげ［＃「ほげ」は大見出し］' => string( [ phrase japanese => [ text 'ほげ' ] ], heading => '大' ),
    'ほげ［＃「ほげ」は中見出し］' => string( [ phrase japanese => [ text 'ほげ' ] ], heading => '中' ),
    'ほげ［＃「ほげ」は窓見出し］' => string( [ phrase japanese => [ text 'ほげ' ] ], heading => '窓' ),

    'ふげ［＃「ふげ」に傍線］'          => string( [ phrase japanese => [ text 'ふげ' ] ], emphasis => '傍線'      ),
    'ふげ［＃「ふげ」に二重傍線］'      => string( [ phrase japanese => [ text 'ふげ' ] ], emphasis => '二重傍線'  ),
    'ふげ［＃「ふげ」に傍点］'          => string( [ phrase japanese => [ text 'ふげ' ] ], emphasis => '傍点'      ),
    'ふげ［＃「ふげ」に白ゴマ傍点］'    => string( [ phrase japanese => [ text 'ふげ' ] ], emphasis => '白ゴマ傍点'),
    'ふげ［＃「ふげ」に丸傍点］'        => string( [ phrase japanese => [ text 'ふげ' ] ], emphasis => '丸傍点'    ),
    'ふげ［＃「ふげ」に白丸傍点］'      => string( [ phrase japanese => [ text 'ふげ' ] ], emphasis => '白丸傍点'  ),
    'ふげ［＃「ふげ」に×傍点］'        => string( [ phrase japanese => [ text 'ふげ' ] ], emphasis => '×傍点'    ),
    'ふげ［＃「ふげ」に黒三角傍点］'    => string( [ phrase japanese => [ text 'ふげ' ] ], emphasis => '黒三角傍点'),
    'ふげ［＃「ふげ」に白三角傍点］'    => string( [ phrase japanese => [ text 'ふげ' ] ], emphasis => '白三角傍点'),
    'ふげ［＃「ふげ」に二重丸傍点］'    => string( [ phrase japanese => [ text 'ふげ' ] ], emphasis => '二重丸傍点'),
    'ふげ［＃「ふげ」に蛇の目傍点］'    => string( [ phrase japanese => [ text 'ふげ' ] ], emphasis => '蛇の目傍点'),
    'ふげ［＃「ふげ」に白四角傍点］'    => string( [ phrase japanese => [ text 'ふげ' ] ], emphasis => '白四角傍点'),
    'ふげ［＃「ふげ」の左に傍線］'      => string( [ phrase japanese => [ text 'ふげ' ] ], left_emphasis => '傍線' ),
    
    'むげ［＃「むげ」は太字］'                              => string( [ phrase japanese => [ text 'むげ' ] ], bold          => 1 ),
    'むげ［＃「むげ」は斜体］'                              => string( [ phrase japanese => [ text 'むげ' ] ], italic        => 1 ),
    'むげ［＃「むげ」は分数］'                              => string( [ phrase japanese => [ text 'むげ' ] ], fraction      => 1 ),
    'むげ［＃「むげ」は下線］'                              => string( [ phrase japanese => [ text 'むげ' ] ], underline     => 1 ),
    'むげ［＃「むげ」は上付き小文字］'                      => string( [ phrase japanese => [ text 'むげ' ] ], superscript   => 1 ),
    'むげ［＃「むげ」は下付き小文字］'                      => string( [ phrase japanese => [ text 'むげ' ] ], subscript     => 1 ),
    'むげ［＃「むげ」は底本では変体仮名で「一」の草書］'    => string( [ phrase japanese => [ text 'むげ' ] ], variantkana   => text('一') ),
    'むげ［＃「むげ」は篆書体］'                            => string( [ phrase japanese => [ text 'むげ' ] ], tensho        => 1,),
    'むげ［＃「むげ」は小書き］'                            => string( [ phrase japanese => [ text 'むげ' ] ], small         => 1 ),
    'むげ［＃「むげ」は行右小書き］'                        => string( [ phrase japanese => [ text 'むげ' ] ], lineside      => '右' ),
    'むげ［＃「むげ」は行左小書き］'                        => string( [ phrase japanese => [ text 'むげ' ] ], lineside      => '左' ),
    'むげ［＃「むげ」は横組み］'                            => string( [ phrase japanese => [ text 'むげ' ] ], horizontal    => 1 ),
    'むげ［＃「むげ」は縦中横］'                            => string( [ phrase japanese => [ text 'むげ' ] ], hinv          => 1 ),
    'むげ［＃「むげ」はローマ数字］'                        => string( [ phrase japanese => [ text 'むげ' ] ], roman_number  => 1 ),
    'むげ［＃「むげ」は合字］'                              => string( [ phrase japanese => [ text 'むげ' ] ], ligature      => 1 ),
    'むげ［＃「むげ」は1段階大きな文字］'                   => string( [ phrase japanese => [ text 'むげ' ] ], fontsize      => 1 ),
    'むげ［＃「むげ」は1段階小さな文字］'                   => string( [ phrase japanese => [ text 'むげ' ] ], fontsize      => -1,),
    'むげ［＃「むげ」は二重罫囲み］'                        => string( [ phrase japanese => [ text 'むげ' ] ], linearborder  => 2 ),
    'むげ［＃「むげ」は罫囲み］'                            => string( [ phrase japanese => [ text 'むげ' ] ], linearborder  => 1 ),

    'もげ［＃「もげ」に「ふふふ」の注記］'                  => string( [  phrase japanese => [ text 'もげ' ] ], annotation => string([ phrase japanese => [ text 'ふふふ' ] ]) ),
    'もげ［＃「もげ」の左に「ふふふ」の注記］'              => string( [  phrase japanese => [ text 'もげ' ] ], left_annotation => string([ phrase japanese => [ text 'ふふふ' ] ]) ),
    'もげ［＃「もげ」は底本では「ふふふ」］'                => string( [  phrase japanese => [ text 'もげ' ] ], original => string([ phrase japanese => [ text 'ふふふ' ] ]) ),
    'もげ［＃「もげ」はママ］'                              => string( [  phrase japanese => [ text 'もげ' ] ], original => string([ phrase japanese => [ text 'ママ' ] ]) ),
);

while ( my ( $source, $node ) = splice @tests, 0, 2 ) {
    is(
        $node->as_string,
        $source,
        $enc->encode($source),
    );
}

my $node = string( [ phrase japanese => [ text 'foo' ] ] );

$node->unshift_contents( phrase japanese => [ text 'baz' ] );
$node->push_contents( phrase japanese => [ text 'bar' ] );

is(
    $node->shift_contents->as_string,
    'baz'
);

is(
    $node->pop_contents->as_string,
    'bar'
);


done_testing;
