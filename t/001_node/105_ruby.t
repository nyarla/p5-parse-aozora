#!perl

use strict;
use warnings;
use utf8;

use Test::More;
use Encode;
use Parse::Aozora::Node::Declare;

BEGIN {
    use_ok('Parse::Aozora::Node::Ruby');
}

my $enc = find_encoding('utf-8');

my @tests = (
    '｜ほげ《ほげ》［＃ルビの「ほげ」は大見出し］' => ruby( target => string([ phrase japanese => [ text 'ほげ' ] ]), ruby => string([ phrase japanese => [ text 'ほげ' ] ]), heading => '大' ),
    '｜ほげ《ほげ》［＃ルビの「ほげ」は中見出し］' => ruby( target => string([ phrase japanese => [ text 'ほげ' ] ]), ruby => string([ phrase japanese => [ text 'ほげ' ] ]), heading => '中' ),
    '｜ほげ《ほげ》［＃ルビの「ほげ」は窓見出し］' => ruby( target => string([ phrase japanese => [ text 'ほげ' ] ]), ruby => string([ phrase japanese => [ text 'ほげ' ] ]), heading => '窓' ),

    '｜ふげ《ふげ》［＃ルビの「ふげ」に傍線］'          => ruby( target => string([ phrase japanese => [ text 'ふげ' ] ]), ruby => string([ phrase japanese => [ text 'ふげ' ] ]), emphasis => '傍線'      ),
    '｜ふげ《ふげ》［＃ルビの「ふげ」に二重傍線］'      => ruby( target => string([ phrase japanese => [ text 'ふげ' ] ]), ruby => string([ phrase japanese => [ text 'ふげ' ] ]), emphasis => '二重傍線'  ),
    '｜ふげ《ふげ》［＃ルビの「ふげ」に傍点］'          => ruby( target => string([ phrase japanese => [ text 'ふげ' ] ]), ruby => string([ phrase japanese => [ text 'ふげ' ] ]), emphasis => '傍点'      ),
    '｜ふげ《ふげ》［＃ルビの「ふげ」に白ゴマ傍点］'    => ruby( target => string([ phrase japanese => [ text 'ふげ' ] ]), ruby => string([ phrase japanese => [ text 'ふげ' ] ]), emphasis => '白ゴマ傍点'),
    '｜ふげ《ふげ》［＃ルビの「ふげ」に丸傍点］'        => ruby( target => string([ phrase japanese => [ text 'ふげ' ] ]), ruby => string([ phrase japanese => [ text 'ふげ' ] ]), emphasis => '丸傍点'    ),
    '｜ふげ《ふげ》［＃ルビの「ふげ」に白丸傍点］'      => ruby( target => string([ phrase japanese => [ text 'ふげ' ] ]), ruby => string([ phrase japanese => [ text 'ふげ' ] ]), emphasis => '白丸傍点'  ),
    '｜ふげ《ふげ》［＃ルビの「ふげ」に×傍点］'        => ruby( target => string([ phrase japanese => [ text 'ふげ' ] ]), ruby => string([ phrase japanese => [ text 'ふげ' ] ]), emphasis => '×傍点'    ),
    '｜ふげ《ふげ》［＃ルビの「ふげ」に黒三角傍点］'    => ruby( target => string([ phrase japanese => [ text 'ふげ' ] ]), ruby => string([ phrase japanese => [ text 'ふげ' ] ]), emphasis => '黒三角傍点'),
    '｜ふげ《ふげ》［＃ルビの「ふげ」に白三角傍点］'    => ruby( target => string([ phrase japanese => [ text 'ふげ' ] ]), ruby => string([ phrase japanese => [ text 'ふげ' ] ]), emphasis => '白三角傍点'),
    '｜ふげ《ふげ》［＃ルビの「ふげ」に二重丸傍点］'    => ruby( target => string([ phrase japanese => [ text 'ふげ' ] ]), ruby => string([ phrase japanese => [ text 'ふげ' ] ]), emphasis => '二重丸傍点'),
    '｜ふげ《ふげ》［＃ルビの「ふげ」に蛇の目傍点］'    => ruby( target => string([ phrase japanese => [ text 'ふげ' ] ]), ruby => string([ phrase japanese => [ text 'ふげ' ] ]), emphasis => '蛇の目傍点'),
    '｜ふげ《ふげ》［＃ルビの「ふげ」に白四角傍点］'    => ruby( target => string([ phrase japanese => [ text 'ふげ' ] ]), ruby => string([ phrase japanese => [ text 'ふげ' ] ]), emphasis => '白四角傍点'),
    
    '｜むげ《むげ》［＃ルビの「むげ」は太字］'                              => ruby( target => string([ phrase japanese => [ text 'むげ' ] ]), ruby => string([ phrase japanese => [ text 'むげ' ] ]), bold          => 1 ),
    '｜むげ《むげ》［＃ルビの「むげ」は斜体］'                              => ruby( target => string([ phrase japanese => [ text 'むげ' ] ]), ruby => string([ phrase japanese => [ text 'むげ' ] ]), italic        => 1 ),
    '｜むげ《むげ》［＃ルビの「むげ」は分数］'                              => ruby( target => string([ phrase japanese => [ text 'むげ' ] ]), ruby => string([ phrase japanese => [ text 'むげ' ] ]), fraction      => 1 ),
    '｜むげ《むげ》［＃ルビの「むげ」は下線］'                              => ruby( target => string([ phrase japanese => [ text 'むげ' ] ]), ruby => string([ phrase japanese => [ text 'むげ' ] ]), underline     => 1 ),
    '｜むげ《むげ》［＃ルビの「むげ」は上付き小文字］'                      => ruby( target => string([ phrase japanese => [ text 'むげ' ] ]), ruby => string([ phrase japanese => [ text 'むげ' ] ]), superscript   => 1 ),
    '｜むげ《むげ》［＃ルビの「むげ」は下付き小文字］'                      => ruby( target => string([ phrase japanese => [ text 'むげ' ] ]), ruby => string([ phrase japanese => [ text 'むげ' ] ]), subscript     => 1 ),
    '｜むげ《むげ》［＃ルビの「むげ」は底本では変体仮名で「一」の草書］'    => ruby( target => string([ phrase japanese => [ text 'むげ' ] ]), ruby => string([ phrase japanese => [ text 'むげ' ] ]), variantkana   => text('一') ),
    '｜むげ《むげ》［＃ルビの「むげ」は篆書体］'                            => ruby( target => string([ phrase japanese => [ text 'むげ' ] ]), ruby => string([ phrase japanese => [ text 'むげ' ] ]), tensho        => 1,),
    '｜むげ《むげ》［＃ルビの「むげ」は小書き］'                            => ruby( target => string([ phrase japanese => [ text 'むげ' ] ]), ruby => string([ phrase japanese => [ text 'むげ' ] ]), small         => 1 ),
    '｜むげ《むげ》［＃ルビの「むげ」は行右小書き］'                        => ruby( target => string([ phrase japanese => [ text 'むげ' ] ]), ruby => string([ phrase japanese => [ text 'むげ' ] ]), lineside      => '右' ),
    '｜むげ《むげ》［＃ルビの「むげ」は行左小書き］'                        => ruby( target => string([ phrase japanese => [ text 'むげ' ] ]), ruby => string([ phrase japanese => [ text 'むげ' ] ]), lineside      => '左' ),
    '｜むげ《むげ》［＃ルビの「むげ」は横組み］'                            => ruby( target => string([ phrase japanese => [ text 'むげ' ] ]), ruby => string([ phrase japanese => [ text 'むげ' ] ]), horizontal    => 1 ),
    '｜むげ《むげ》［＃ルビの「むげ」は縦中横］'                            => ruby( target => string([ phrase japanese => [ text 'むげ' ] ]), ruby => string([ phrase japanese => [ text 'むげ' ] ]), hinv          => 1 ),
    '｜むげ《むげ》［＃ルビの「むげ」はローマ数字］'                        => ruby( target => string([ phrase japanese => [ text 'むげ' ] ]), ruby => string([ phrase japanese => [ text 'むげ' ] ]), roman_number  => 1 ),
    '｜むげ《むげ》［＃ルビの「むげ」は合字］'                              => ruby( target => string([ phrase japanese => [ text 'むげ' ] ]), ruby => string([ phrase japanese => [ text 'むげ' ] ]), ligature      => 1 ),
    '｜むげ《むげ》［＃ルビの「むげ」は1段階大きな文字］'                   => ruby( target => string([ phrase japanese => [ text 'むげ' ] ]), ruby => string([ phrase japanese => [ text 'むげ' ] ]), fontsize      => 1 ),
    '｜むげ《むげ》［＃ルビの「むげ」は1段階小さな文字］'                   => ruby( target => string([ phrase japanese => [ text 'むげ' ] ]), ruby => string([ phrase japanese => [ text 'むげ' ] ]), fontsize      => -1,),
    '｜むげ《むげ》［＃ルビの「むげ」は二重罫囲み］'                        => ruby( target => string([ phrase japanese => [ text 'むげ' ] ]), ruby => string([ phrase japanese => [ text 'むげ' ] ]), linearborder  => 2 ),
    '｜むげ《むげ》［＃ルビの「むげ」は罫囲み］'                            => ruby( target => string([ phrase japanese => [ text 'むげ' ] ]), ruby => string([ phrase japanese => [ text 'むげ' ] ]), linearborder  => 1 ),

    '｜もげ《もげ》［＃ルビの「もげ」に「ふふふ」の注記］'                  => ruby( target => string([  phrase japanese => [ text 'もげ' ] ]), ruby => string([  phrase japanese => [ text 'もげ' ] ]), annotation => string([ phrase japanese => [ text 'ふふふ' ] ]) ),
    '｜もげ《もげ》［＃ルビの「もげ」の左に「ふふふ」の注記］'              => ruby( target => string([  phrase japanese => [ text 'もげ' ] ]), ruby => string([  phrase japanese => [ text 'もげ' ] ]), left_annotation => string([ phrase japanese => [ text 'ふふふ' ] ]) ),
    '｜もげ《もげ》［＃ルビの「もげ」は底本では「ふふふ」］'                => ruby( target => string([  phrase japanese => [ text 'もげ' ] ]), ruby => string([  phrase japanese => [ text 'もげ' ] ]), original => string([ phrase japanese => [ text 'ふふふ' ] ]) ),
    '｜もげ《もげ》［＃ルビの「もげ」はママ］'                              => ruby( target => string([  phrase japanese => [ text 'もげ' ] ]), ruby => string([  phrase japanese => [ text 'もげ' ] ]), original => string([ phrase japanese => [ text 'ママ' ] ]) ),

    '｜はげ《はげ》'                       => ruby( target => string([ phrase japanese => [ text 'はげ' ] ]), ruby => string([ phrase japanese => [ text 'はげ' ] ]) ),
    '［＃「はげ」の左に「はげ」のルビ］'   => ruby( target => string([ phrase japanese => [ text 'はげ' ] ]), left_ruby => string([ phrase japanese => [ text 'はげ' ] ]) ), 
);

while ( my ( $source, $node ) = splice @tests, 0, 2 ) {
    is(
        $node->as_string,
        $source,
        $enc->encode($source),
    );
}

done_testing;

