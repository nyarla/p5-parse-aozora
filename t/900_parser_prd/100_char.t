#!perl

use strict;
use warnings;
use utf8;

use Test::More;
use Parse::Aozora::Node::Declare;
use Parse::Aozora::Parser::PRD;

my $parser = Parse::Aozora::Parser::PRD->new;

# EUPhrase
subtest 'EUPhrase' => sub {
    is_deeply(
        $parser->EUPhrase(q|〔english text〕|),
        phrase europea => [ text 'english text' ],
    );
    
    done_testing;
};

# CHPrase
subtest 'KaeriTen' => sub {
    is_deeply(
        $parser->KaeriTen(q|［＃一レ］|),
        punctuation kaeriten => [ text '一レ'],
    );

    is_deeply(
        $parser->KaeriTen(q|［＃レ］|),
        punctuation kaeriten => [ text 'レ'],
    );

    is_deeply(
        $parser->KaeriTen(q|［＃二］|),
        punctuation kaeriten => [ text '二' ],
    );
    
    done_testing;
};

subtest 'KuntenOkuri'  => sub {
    is_deeply(
        $parser->KuntenOkuri(q|［＃（ノ）］|),
        punctuation kuntenokuri => [ text 'ノ' ],
    );

    is_deeply(
        $parser->KuntenOkuri(q|［＃（弖）］|),
        punctuation kuntenokuri => [ text '弖' ],
    );

    done_testing;
};

subtest 'CNPhrase' => sub {
    is_deeply(
        $parser->CNPhrase(q|［＃一］|),
        phrase chineseclassic => [ punctuation kaeriten => [ text '一' ] ],
    );

    is_deeply(
        $parser->CNPhrase(q|［＃（ヲ）］|),
        phrase chineseclassic => [ punctuation kuntenokuri => [ text 'ヲ' ] ],
    );

    is_deeply(
        $parser->CNPhrase(q|［＃（ヲ）］［＃一］|),
        phrase chineseclassic => [
            punctuation( kuntenokuri => [ text 'ヲ' ] ),
            punctuation( kaeriten    => [ text '一' ] ),
        ],
    );

    done_testing;
};

# Extra

subtest 'ExtraContents' => sub {
    is(
        $parser->ExtraCharDescription(q|てへん＋劣|),
        'てへん＋劣',
    );

    is(
        $parser->ExtraCharCode(q|二の字点|),
        '二の字点',
    );

    done_testing;
};

subtest 'ExtraKanjiChar' => sub {

    is_deeply(
        $parser->ExtraKanjiChar(q|※［＃「てへん＋劣」、第3水準1-84-77］|),
        extrachar 'kanji', 'てへん＋劣', '第3水準1-84-77'
    );
    
    done_testing;
};

subtest 'ExtraChar' => sub {
    is_deeply(
        $parser->ExtraChar(q|※［＃二の字点、1-2-22］|),
        extrachar 'char', '二の字点', '1-2-22',
    );

    done_testing;
};

# Phrase
subtest 'Phrase' => sub {
    is_deeply(
        $parser->Phrase(q|あいうえお|),
        phrase japanese => [text 'あいうえお'],
    );

    done_testing;
};

# String
subtest 'String' => sub {
    is_deeply(
        $parser->String(q|あいうえお|),
        string( [ phrase japanese => [ text 'あいうえお' ] ] ),
    );
    
    is_deeply(
        $parser->String(q|※［＃「てへん＋劣」、第3水準1-84-77］|),
        string( [ phrase japanese => [ extrachar 'kanji', 'てへん＋劣', '第3水準1-84-77' ] ] ),
    );
    
    is_deeply(
        $parser->String(q|※［＃二の字点、1-2-22］|),
        string( [ phrase japanese => [ extrachar 'char', '二の字点', '1-2-22' ] ] ),
    );
    is_deeply(
        $parser->String(q|〔english〕|),
        string( [ phrase europea => [ text 'english' ] ] ),
    );
    
    is_deeply(
        $parser->String(q|［＃（ヲ）］［＃一］|),
        string( [ phrase chineseclassic => [
            punctuation( kuntenokuri    => [ text 'ヲ' ] ),
            punctuation( kaeriten       => [ text '一' ] ),
        ] ] )
    );

    is_deeply(
        $parser->String(q|englishあいう※［＃「てへん＋劣」、第3水準1-84-77］※［＃二の字点、1-2-22］〔english〕［＃（ヲ）］［＃一］|),
        string( [
            phrase( japanese => [
                text( 'englishあいう'),
                extrachar( 'kanji', 'てへん＋劣', '第3水準1-84-77' ),
                extrachar( 'char', '二の字点', '1-2-22' ),
            ] ),
            phrase( europea => [ text 'english' ] ),
            phrase( chineseclassic => [
                punctuation( 'kuntenokuri' => [ text 'ヲ' ] ),
                punctuation( 'kaeriten', => [ text '一' ] ),
            ] )   ,
        ] )
    );

    done_testing;
};

done_testing;
