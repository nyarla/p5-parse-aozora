#!perl

use strict;
use warnings;
use utf8;

use Encode;

use Test::More;
use Parse::Aozora::Parser::PRD;
use Parse::Aozora::Node::Declare;

my $parser = Parse::Aozora::Parser::PRD->new;
my $enc     = find_encoding('UTF-8');

subtest 'ModifyAnnotation' => sub {
    my @tests = (
        'ほげ［＃「ほげ」は大見出し］'  => string([ phrase japanese => [ text('ほげ') ] ], heading => '大'),
        'ほげ［＃「ほげ」は中見出し］'  => string([ phrase japanese => [ text('ほげ') ] ], heading => '中'),
        'ほげ［＃「ほげ」は窓見出し］'  => string([ phrase japanese => [ text('ほげ') ] ], heading => '窓'),
        
        'ふげ［＃「ふげ」の左に傍線］'          => string([ phrase japanese => [text('ふげ')] ],     left_emphasis   => '傍線'      ),
        'ふげ［＃「ふげ」の左に二重傍線］'      => string([ phrase japanese => [text('ふげ')] ],     left_emphasis   => '二重傍線'  ),
        'ふげ［＃「ふげ」の左に傍点］'          => string([ phrase japanese => [text('ふげ')] ],     left_emphasis   => '傍点'      ),
        'ふげ［＃「ふげ」の左に白ゴマ傍点］'    => string([ phrase japanese => [text('ふげ')] ],     left_emphasis  => '白ゴマ傍点'),
        'ふげ［＃「ふげ」の左に丸傍点］'        => string([ phrase japanese => [text('ふげ')] ],     left_emphasis  => '丸傍点'    ),
        'ふげ［＃「ふげ」の左に白丸傍点］'      => string([ phrase japanese => [text('ふげ')] ],     left_emphasis  => '白丸傍点'  ),
        'ふげ［＃「ふげ」に×傍点］'            => string([ phrase japanese => [text('ふげ')] ],     emphasis => '×傍点'),
        'ふげ［＃「ふげ」に黒三角傍点］'        => string([ phrase japanese => [text('ふげ')] ],     emphasis => '黒三角傍点'),
        'ふげ［＃「ふげ」に白三角傍点］'        => string([ phrase japanese => [text('ふげ')] ],     emphasis => '白三角傍点'),
        'ふげ［＃「ふげ」に二重丸傍点］'        => string([ phrase japanese => [text('ふげ')] ],     emphasis => '二重丸傍点'),
        'ふげ［＃「ふげ」に蛇の目傍点］'        => string([ phrase japanese => [text('ふげ')] ],     emphasis => '蛇の目傍点'),
        'ふげ［＃「ふげ」に白四角傍点］'        => string([ phrase japanese => [text('ふげ')] ],     emphasis => '白四角傍点'),
   
        'むげ［＃「むげ」は太字］'                              => string([ phrase japanese => [ text('むげ')] ], bold        => 1 ),
        'むげ［＃「むげ」は斜体］'                              => string([ phrase japanese => [ text('むげ')] ], italic      => 1 ),
        'むげ［＃「むげ」は分数］'                              => string([ phrase japanese => [ text('むげ')] ], fraction    => 1 ),
        'むげ［＃「むげ」は下線］'                              => string([ phrase japanese => [ text('むげ')] ], underline   => 1 ),
        'むげ［＃「むげ」は上付き小文字］'                      => string([ phrase japanese => [ text('むげ')] ], superscript => 1 ),
        'むげ［＃「むげ」は下付き小文字］'                      => string([ phrase japanese => [ text('むげ')] ], subscript   => 1 ),
        'むげ［＃「むげ」は底本では変体仮名で「一」の草書］'    => string([ phrase japanese => [ text('むげ')] ], variantkana => text('一') ),
        'むげ［＃「むげ」は篆書体］'                            => string([ phrase japanese => [ text('むげ')] ], tensho      => 1,),
        'むげ［＃「むげ」は小書き］'                            => string([ phrase japanese => [ text('むげ')] ], small       => 1 ),
        'むげ［＃「むげ」は行右小書き］'                        => string([ phrase japanese => [ text('むげ')] ], lineside    => '右' ),
        'むげ［＃「むげ」は行左小書き］'                        => string([ phrase japanese => [ text('むげ')] ], lineside    => '左' ),
        'むげ［＃「むげ」は横組み］'                            => string([ phrase japanese => [ text('むげ')] ], horizontal  => 1 ),
        'むげ［＃「むげ」は縦中横］'                            => string([ phrase japanese => [ text('むげ')] ], hinv        => 1 ),
        'むげ［＃「むげ」はローマ数字］'                        => string([ phrase japanese => [ text('むげ')] ], roman_number=> 1 ),
        'むげ［＃「むげ」は合字］'                              => string([ phrase japanese => [ text('むげ')] ], ligature    => 1 ),
        'むげ［＃「むげ」は一段階大きな文字］'                  => string([ phrase japanese => [ text('むげ')] ], fontsize    => 1 ),
        'むげ［＃「むげ」は1段階小さな文字］'                   => string([ phrase japanese => [ text('むげ')] ], fontsize    => -1,),
        'むげ［＃「むげ」は２段階大きな文字］'                  => string([ phrase japanese => [ text('むげ')] ], fontsize    => 2 ),
        'むげ［＃「むげ」は二重罫囲み］'                        => string([ phrase japanese => [ text('むげ')] ], linearborder=> 2 ),
        'むげ［＃「むげ」は罫囲み］'                            => string([ phrase japanese => [ text('むげ')] ], linearborder=> 1 ),
        
        'はげ［＃「はげ」に「ふげ」の注記］'                    => string([ phrase japanese => [ text('はげ')] ], annotation => string([ phrase japanese => [ text 'ふげ' ]  ]) ),
        'はげ［＃「はげ」の左に「ふげ」の注記］'                => string([ phrase japanese => [ text('はげ')] ], left_annotation => string([ phrase japanese => [ text 'ふげ' ]  ]) ),
    
        'はげ［＃「はげ」は底本では「はげげ」］'                => string([ phrase japanese => [ text('はげ')] ], original => string([ phrase japanese => [ text 'はげげ' ] ]) ),
        'はけ［＃「はけ」はママ］'                              => string([ phrase japanese => [ text('はけ') ] ], original => string([ phrase japanese => [ text 'ママ' ] ]), ),
    );

    while ( my ( $source, $result ) = splice @tests, 0, 2 ) {
        is_deeply(
            $parser->ModifiedString($source),
            $result,
            $enc->encode($source),
        );
    }

    done_testing;
};

subtest 'ModifiedString' => sub {
    is_deeply(
        $parser->ModifiedString(q|あいう［＃「あいう」は太字］［＃「あいう」は斜体］|),
        string( [ phrase( japanese => [ text('あいう') ] ) ], bold => 1, italic => 1 ),
    );
};

done_testing;
