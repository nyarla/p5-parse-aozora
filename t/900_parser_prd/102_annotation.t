#!perl

use strict;
use warnings;
use utf8;

use Test::More;
use Parse::Aozora::Parser::PRD;
use Parse::Aozora::Node::Declare;

my $parser = Parse::Aozora::Parser::PRD->new;

subtest 'GeneralRuby' => sub {
    # euphrase
    is_deeply(
        $parser->GeneralRuby(q|〔blosxom〕《ブロッサム》|),
        {
            target  => string([ phrase europea => [ text 'blosxom' ] ]),
            attr    => 'ruby',
            value   => string([ phrase japanese => [ text 'ブロッサム' ] ]),
        }
    );

    # kaji
    is_deeply(
        $parser->GeneralRuby(q|青空文庫《あおぞらぶんこ》|),
        {
            target  => string([ phrase japanese => [ map { text $_ } split m{}, '青空文庫' ] ]),
            value   => string([ phrase japanese => [ text 'あおぞらぶんこ' ] ]),
            attr    => 'ruby',
        },
    );
    
    # hiragana
    is_deeply(
        $parser->GeneralRuby(q|あいうえお《カキクケコ》|),
        {
            target  => string([ phrase japanese => [ text 'あいうえお' ] ]),
            value   => string([ phrase japanese => [ text 'カキクケコ' ] ]),
            attr    => 'ruby',
        },
    );

    # katakana
    is_deeply(
        $parser->GeneralRuby(q|アイウエオ《あいうえお》|),
        {
            target  => string([ phrase japanese => [ text 'アイウエオ' ] ]),
            value   => string([ phrase japanese => [ text 'あいうえお' ] ]),
            attr    => 'ruby',
        },
    );

    done_testing;
};

subtest 'SpecifiedRuby' => sub {
    is_deeply(
        $parser->SpecifiedRuby(q[｜あいうえお《かきくけこ》]),
        {
            target  => string([ phrase japanese => [ text 'あいうえお' ] ]),
            attr    => 'ruby',
            value   => string([ phrase japanese => [ text 'かきくけこ' ] ]),
        }
    );
    
    done_testing;
};

subtest 'AnnotationRuby' => sub {
    is_deeply(
        $parser->AnnotationRuby(q|［＃「あいう」の左に「かきく」のルビ］|),
        {
            target  => string([ phrase japanese => [ text 'あいう' ] ]),
            attr    => 'left_ruby',
            value   => string([ phrase japanese => [ text 'かきく' ] ]),
        }
    );
};

subtest 'RubyText' => sub {

    is_deeply(
        $parser->RubyText(q{｜あいうえお《あいうえお》［＃ルビの「あいうえお」は太字］}),
        ruby(
            target  => string([ phrase japanese => [ text 'あいうえお' ] ]),
            ruby    => string([ phrase japanese => [ text 'あいうえお' ] ]),
            bold    => 1,
        ),
    );

    is_deeply(
        $parser->RubyText(q{あいうえお《あいうえお》［＃ルビの「あいうえお」は底本では「ほげ」］}),
        ruby(
            target      => string([ phrase japanese => [ text 'あいうえお' ] ]),
            ruby        => string([ phrase japanese => [ text 'あいうえお' ] ]),
            original    => string([ phrase japanese => [ text 'ほげ' ] ]),
        )
    );
    
    is_deeply(
        $parser->RubyText(q{｜かきくけこ《かきくけこ》［＃ルビの「かきくけこ」はママ］}),
        ruby(
            target      => string([ phrase japanese => [ text 'かきくけこ' ] ]),
            ruby        => string([ phrase japanese => [ text 'かきくけこ' ] ]),
            original    => string([ phrase japanese => [ text 'ママ' ] ]),
        )
    );

    is_deeply(
        $parser->RubyText(q|あいうえお《あいうえお》|),
        ruby(
            target  => string([ phrase japanese => [ text 'あいうえお' ] ]),
            ruby    => string([ phrase japanese => [ text 'あいうえお' ] ]),
        ),
    );

    is_deeply(
        $parser->RubyText(q[｜これは《きけけ》［＃ルビの「きけけ」は太字］]),
        ruby(
            target  => string([ phrase japanese => [ text 'これは' ] ]),
            ruby    => string([ phrase japanese => [ text 'きけけ' ] ]),
            bold    => 1,
        )
    );

    is_deeply(
        $parser->RubyText(q[｜これは《きけけ》［＃ルビの「きけけ」は太字］［＃ルビの「きけけ」はママ］]),
        ruby(
            target      => string([ phrase japanese => [ text 'これは' ] ]),
            ruby        => string([ phrase japanese => [ text 'きけけ' ] ]),
            bold        => 1,
            original    => string([ phrase japanese => [ text 'ママ' ] ]),
        ),
    );

    done_testing;
};

subtest 'AnnotationString' => sub {

    is_deeply(
        $parser->AnnotationString(q|あいうえお［＃「あいうえお」は太字］|),
        string([ phrase japanese => [ text 'あいうえお' ] ], bold => 1),
    );
    
    is_deeply(
        $parser->AnnotationString(q{｜青空文庫《あおぞらぶんこ》}),
        ruby(
            target  => string([ phrase japanese => [ text '青空文庫' ] ]),
            ruby    => string([ phrase japanese => [ text 'あおぞらぶんこ'] ]),
        ),
    );


    done_testing;
};

done_testing;
